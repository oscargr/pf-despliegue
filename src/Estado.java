
public enum Estado {
	
	OK("Ok"),
	FALLO_LOGIN("No se ha podido iniciar sesi�n:<br/>usuario no registrado o contrase�a incorrecta"),
	ERROR_LOGIN("Un error interno impide el inicio de sesi�n:<br/>contacte con el administrador"),
	FALLO_REGISTRO("No se ha podido registrar el usuario:<br/>el nombre de usuario ya existe"),
	ERROR_REGISTRO("Un error interno impide el registro del usuario:<br/>contacte con el administrador"),
	ERROR_INTERNO("Error interno, contacte con el administrador");
		
	String msg;
	
	private Estado(String msg) {
		this.msg = msg;
	}
	
	@Override
	public String toString() {
		return msg;
	}

}
