import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

public class HTML {
	
	static void mostrarPagina(HttpSession session, HttpServletResponse response, String contenido, String titulo, String css, String js)
			throws IOException {
		PrintWriter out = null;
		try {
			response.setCharacterEncoding("utf-8");
			out = response.getWriter();
			out.println("<!DOCTYPE html>");
			out.println("<html>");
			out.println("<head>");
			out.println("<meta charset=\"UTF-8\">");
			out.printf("<title>%s</title\n>", titulo);
			out.println("<meta name=\"viewport\" content=\"width=device-width, initial-scale=1, shrink-to-fit=no\">");
			out.println("<link rel=\"stylesheet\" href=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css\" integrity=\"sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh\" crossorigin=\"anonymous\">");
			if (css != null)
				out.printf("<link rel=\"stylesheet\" type=\"text/css\" href=\"css/%s.css\" media=\"screen\" />\n", css);
			if (js != null)
				out.printf("<script type=\"text/javascript\" src=\"js/%s.js\"></script>\n", js);
			out.println("</head>");
			out.println("<body>");
			out.println("<nav class=\"navbar navbar-expand-md bg-dark navbar-dark\">");
			out.println("	<a href=\"\" class=\"navbar-brand\">Bootstrap</a>");
			out.println("	<button class=\"navbar-toggler\" type=\"button\" data-toggle=\"collapse\" data-target=\"#collapsibleNavbar\">");
			out.println("		<span class=\"navbar-toggler-icon\"></span>");
			out.println("	</button>");
			out.println("	<div class=\"collapse navbar-collapse\" id=\"collapsibleNavbar\">");
			out.println("		<ul class=\"navbar-nav ml-auto\">");
			if (session == null) {
				out.println("			<li class=\"nav-item mx-3\">");
				out.println("				<a class=\"btn btn-link\" href=\"login\">Iniciar sesi�n</a>");
				out.println("			</li>");
				out.println("			<li class=\"nav-item mx-3\">");
				out.println("				<a class=\"btn btn-link\" href=\"registro\">Registrarse</a>");
				out.println("			</li>");
			}
			else {
				out.println("			<li class=\"nav-item\">");
				out.printf("		%s (<a class=\"btn btn-danger\" href=\"logout\">Cerrar sesi�n</a>)", session.getAttribute("usuario"));
				out.println("			</li>");
			}
			out.println("		</ul>");
			out.println("	</div>");
			out.println("</nav>");
			out.println("<div class=\"container-fluid\">");
			out.println(contenido);
			out.println("</div>");
			out.println("<script src=\"https://code.jquery.com/jquery-3.4.1.slim.min.js\" integrity=\"sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n\" crossorigin=\"anonymous\"></script>");
			out.println("<script src=\"https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js\" integrity=\"sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo\" crossorigin=\"anonymous\"></script>");
			out.println("<script src=\"https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js\" integrity=\"sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6\" crossorigin=\"anonymous\"></script>");
			out.println("</body>");
			out.println("</html>");
		}
		finally {
			if (out != null)
				out.close();
		}
	}
	
	static void enviarInicio(HttpSession session, HttpServletResponse response) throws IOException {
		HTML.mostrarPagina(session, response, "<h1>Hola mundo</h1>", "Inicio", null, null);
	}
	
	static String divError(String mensaje) {
		return String.format("<div class=\"error\"><p>%s</p></div>", mensaje);
	}
	
	public static void enviarFormRegistro(HttpSession session, HttpServletResponse response, String action, String titulo, String id, String txtSubmitting, String txtSubmit, Estado estado) throws IOException {
		StringBuilder html = new StringBuilder();
		titulo = "Registro";
		
		html.append("<div class=\"bg-light w-50 mx-auto shadow-lg rounded-lg p-3\">\n");
		if (estado != Estado.OK)
			html.append(divError(estado.toString()));
		html.append(String.format("<form action=\"%s\" method=\"POST\" onsubmit=\"return validar('%s')\">", action, txtSubmitting));
		html.append("<div class=\"form-group\">\n");
		html.append("<label for=\"id\">Usuario:</label>\n");
		html.append("<input type=\"text\" class=\"form-control\" name=\"id\" placeholder=\"Introduce tu nombre de usuario\" id=\"id\"");
		if (estado != Estado.OK) {
			if (id != null)
				html.append(String.format(" value=\"%s\"", id));
		}
		html.append("/>\n");
		html.append("</div>\n");
		html.append("<div class=\"form-group\">\n");
		html.append("<label for=\"email\">Email:</label>\n");
		html.append("<input type=\"email\" class=\"form-control\" name=\"email\" placeholder=\"Introduce tu email\" id=\"email\">\n");
		html.append("</div>\n");
		html.append("<div class=\"form-group\">\n");
		html.append("<label for=\"pwd\">Contrase�a:</label>\n");
		html.append("<input type=\"password\" class=\"form-control\" name=\"pwd\" placeholder=\"Introduce tu contrase�a\" id=\"pwd\">\n");
		html.append("</div>\n");
		html.append(String.format("<button type=\"submit\" class=\"btn btn-primary\">%s</button>\n", txtSubmit));
		html.append("</form>\n");
		html.append("</div>\n");
		HTML.mostrarPagina(session, response, html.toString(), titulo, null, null);
	}

}
